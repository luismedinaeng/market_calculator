# Readme file for project market calculator in php
Program that computes the price of a product  depending on its original price, adding an specific tax

## Standard

- Centered titles using Verdana as font. Bolding and blue text
- Use text boxes for input textes
- Output will be in plain text in Times New Roman font
- Just using a general function

## Author

Luis Medina [GitLab](http://gitlab.com/luismedinaeng) [GitHub](http://github.com/luismedinaeng)
